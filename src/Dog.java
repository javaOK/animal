public class Dog extends Animal{

    final int MAX_RUN = 500;
    final int MAX_SWIM = 10;
    static int COUNT_DOG = 0;
    private String name;

    //constructor
    public Dog(String name) {
        super();
        this.name = name;
        COUNT_DOG++;
    }
    @Override
    public void swim(int distToSwim){

        if(distToSwim > MAX_SWIM){
            System.out.println("Dogs can swim 10m only.");
            return;
        }
        else if (distToSwim < 0) {
            System.out.println("Error!The distance isn't valid.");
            return;
        }

        System.out.println(getName() + " swam " + distToSwim + "m." );

    }
    @Override
    public void run(int distToRun){

        if(distToRun > MAX_RUN){
            System.out.println("Dogs can run 500m only.");
            return;
        }
        else if (distToRun < 0) {
            System.out.println("Error!The distance isn't valid.");
            return;
        }

        System.out.println(getName() + " run " + distToRun + "m." );

    }

    public int getCountDog() {
        return COUNT_DOG;
    }

    public String getName() {
        return name;
    }

}
