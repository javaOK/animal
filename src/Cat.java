public class Cat extends Animal {

    final int MAX_RUN = 200;
    static int COUNT_CAT = 0;
    private String name;

    public Cat(String name) {
        super();
        this.name = name;
        COUNT_CAT++;
    }
    @Override
    public void swim(int distToSwim){

        System.out.println("Cats cannot swim.");

    }
    @Override
    public void run(int distToRun){
        if(distToRun > MAX_RUN){
            System.out.println("Cats can run 200m only.");
            return;
        }
        else if (distToRun < 0) {
            System.out.println("Error!The distance to run isn't valid.");
            return;
        }

        System.out.println(getName() +" run " + distToRun + "m." );

    }

    public int getCountCat() {
        return COUNT_CAT;
    }

    public String getName() {
        return name;
    }

}
