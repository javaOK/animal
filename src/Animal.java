public abstract class Animal {

    static int COUNT_ANIMAL = 0;

    public Animal(){
        COUNT_ANIMAL++;
    }
    //methods
    public abstract void swim(int lenght_Obstacle);

    public abstract void run(int lenght_Obstacle);

    public int getCountAnimal() {
        return COUNT_ANIMAL;
    }

}
