public class Main {
    public static void main(String[] args) {

        Animal dog1 = new Dog("Bobik");
        Dog dog2 = new Dog("Sharik");
        Animal cat1 = new Cat("Murka");
        Cat cat2 = new Cat("Ketty");

        cat1.run(100);
        cat1.run(209);
        cat1.run(0);
        cat2.swim(40);

        dog1.run(600);
        dog2.run(-1);
        dog2.swim(14);
        dog1.swim(4);
        System.out.println("Quantity of Animals - " + dog2.getCountAnimal() +
                "\nQuantity of Dogs - " + dog2.getCountDog() +
                "\nQuantity of Cats - " + cat2.getCountCat());

    }
}